import * as React from "react";
import { Route } from "react-router-dom";
import Counter from "./views/Counter/Counter";
import FetchData from "./views/FetchData/FetchData";
import Home from "./views/Home/Home";
import { Layout } from "./components/Layout";

export const routes = (
  <Layout>
    <Route exact path="/" component={Home} />
    <Route path="/counter" component={Counter} />
    <Route path="/fetchdata/:startDateIndex?" component={FetchData} />
  </Layout>
);
